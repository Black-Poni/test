# build environment
FROM node:14-alpine as builder
WORKDIR /app
ENV PATH /app/node_modules/.bin:$PATH

# copy React source codes into the build image
COPY package.json ./
#COPY package-lock.json ./
COPY src ./src
COPY public ./public

# install NPM packages according to package-lock.json
RUN npm ci

# this param needs to be supplied as --build-arg when building the image
ARG API_URL
# building the react application using the param
RUN REACT_APP_API_URL=$API_URL npm run build
